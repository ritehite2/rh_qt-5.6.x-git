#!/bin/bash

# Deploy script for qt
# Deploys only to the app partition

set -e

cd $(dirname $0)

DEFAULT_QT_DEPLOY_PATH=../deploy/app
DEFAULT_ROOTFS_DEPLOY_PATH=../deploy/rootfs
QT_ENV_SH_PATH=/etc/profile.d
QT_BUILD_OUTPUT_PATH=/home/beta/QT_toolchains/target
NUM_ARGS=$#
STARTING_LOCATION="$PWD"

if [ "$NUM_ARGS" -eq "1" ]
then
	DEPLOY_PATH="$1"
else
	DEPLOY_PATH="$DEFAULT_QT_DEPLOY_PATH"
fi

if [ ! -e "$DEPLOY_PATH" ]
then
	echo "Deploy path: "$DEPLOY_PATH" does not exist, exiting"
	exit 1
fi

if [ ! -e "$DEFAULT_ROOTFS_DEPLOY_PATH""$QT_ENV_SH_PATH" ]
then 
	echo "Path to qt_env.sh not correct: "$DEFAULT_ROOTFS_DEPLOY_PATH""$QT_ENV_SH_PATH", exiting"
	exit 1
fi

if [ ! -e "$QT_BUILD_OUTPUT_PATH" ]
then
	echo "A build not does appear to have been run, exiting"
	exit 1
fi

echo "#### Creating deploy folders ####"
echo ""
if [ ! -e "$DEPLOY_PATH"/current ]
then
	mkdir "$DEPLOY_PATH"/current
fi

#Builds of app directory supply this with the needed virtual keyboard changes
#if [ ! -e "$DEPLOY_PATH"/current/opt ]
#then
#	mkdir "$DEPLOY_PATH"/current/opt
#fi
#
#echo "#### Deploying Qt ####"
#echo ""
#if [ -e "$DEPLOY_PATH"/current/opt/qt5 ]
#then
#	echo "QT deployed previously, clear deploy path, exiting"
#	exit 1
#fi
#cp -r "$QT_BUILD_OUTPUT_PATH" "$DEPLOY_PATH"/current/opt
#sync

echo "#### Copy qt_env.sh to profile.d ####"
echo ""
cp qt_env.sh "$DEFAULT_ROOTFS_DEPLOY_PATH""$QT_ENV_SH_PATH"

echo "#### Creating link for rootfs ####"
echo ""
cd "$DEFAULT_ROOTFS_DEPLOY_PATH"/mnt
if [ ! -e app ]
then
	mkdir app
	cd ..
	ln -s /mnt/app/current/opt/ opt
else
	cd ..
	ln -sf /mnt/app/current/opt/ opt
fi
echo "Deploy qt5.6 - COMPLETE"
echo ""
exit 0
