#!/bin/sh

### QT Environment Variables ###
export QT_QPA_GENERIC_PLUGINS=Auto
export QT_QPA_EVDEV_TOUCHSCREEN_PARAMETERS=
export QT_QPA_EGLFS_TSLIB="1"
#export QT_QPA_EGLFS_PHYSICAL_WIDTH="155"	These physical parameters resolved warnings but made the screen look worse
#export QT_QPA_EGLFS_PHYSICAL_HEIGHT="86"
#export QT_SELECT=/opt/qt5	this setting was originally in an environment script, was being overwritten somehow
#export QT_QPA_GENERIC_PLUGINS=/opt/qt5/lib	this setting was originally in an environment script, was being overwritten by the setting in this file

